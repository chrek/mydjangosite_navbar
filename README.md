**ThIs is a Django Project with a "polls" app and a "pages" app**

**Polls app**

The "polls" app is a basic poll application that allows people to view polls and vote in them.

The Web-poll application is setup to use a simple form processing.
The form displays a question and the user is expected to make a choice from a list of choices
and to submit the form by clicking a button.

**Pages app**

The "pages" app is a basic Django app with active bootstrap navigation.
The app is configured to serve static files and has three pages (Home, About and Contact) that use a common base template.
The layout of each page contains a bootstrap 4 based navbar and a base template. 
This is possible by employing Django's template inheritance features.

**Files:**

* urls.py - A URL mapper is typically stored in this file. 

* views.py - in this file, we define the function(s) that take(s) a Web 
  request and return(s) a Web response.

* Defining data models (models.py). Django web applications manage and query data through Python objects referred to as models. 
    The model is the object that is mapped to the database

* Templates: A template is a text file defining the structure or layout of a file (such as an HTML page), with placeholders 
  used to represent actual content. A view can dynamically create an HTML page using an HTML template, populating it with data from a model. A template can be used to define the structure of any type of file; it doesn't have to be HTML!
---

**The development server**

Change into the outer mydjangosite directory and run the following commands:
`py manage.py runserver`
Then, go to /polls/1/ in your browser and vote in the question. You should see a results page that gets updated each time you vote. 

## Edit a file

If you need to edit yoru README file or any file in Bitbucket.

1. Click **Source** on the left side.
2. Click the file filename (in this case, README.md) link from the list of files.
3. Click the **Edit** button.
4. Make your changes
5. After making your change, click **Commit** and then **Commit** again in the dialog. The commit page will open and you’ll see the change you just made.
6. Go back to the **Source** page.

---

## Create a file

If you need to add a new file to your repository.

1. Click the **New file** button at the top of the **Source** page.
2. Give the file a filename of **comments.txt**.
3. Enter your name in the empty file space.
4. Click **Commit** and then **Commit** again in the dialog.
5. Go back to the **Source** page.

You can also have a look at other pages (**Commits**, **Branches**, and **Settings**) in an effort to explore the repository.

---

## Clone a repository

Use these steps to clone from SourceTree, our client for using the repository command-line free. Cloning allows you to work on your files locally. If you don't yet have SourceTree, [download and install first](https://www.sourcetreeapp.com/). If you prefer to clone from the command line, see [Clone a repository](https://confluence.atlassian.com/x/4whODQ).

1. You’ll see the clone button under the **Source** heading. Click that button.
2. Now click **Check out in SourceTree**. You may need to create a SourceTree account or log in.
3. When you see the **Clone New** dialog in SourceTree, update the destination path and name if you’d like to and then click **Clone**.
4. Open the directory you just created to see your repository’s files.

With your Bitbucket repository ready, go ahead and add a new file locally. You can [push your change back to Bitbucket with SourceTree](https://confluence.atlassian.com/x/iqyBMg), or you can [add, commit,](https://confluence.atlassian.com/x/8QhODQ) and [push from the command line](https://confluence.atlassian.com/x/NQ0zDQ).

## References

1. [Django](https://docs.djangoproject.com/en/2.1/intro/tutorial01/)
2. [Medium](https://medium.com/@dustindavignon/django-2-0-make-clicked-tab-active-with-bootstrap-de27a74f6b76)
3. [W3Schools](https://www.w3schools.com/css/css3_buttons.asp)