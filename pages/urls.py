from django.urls import path

from . import views

urlpatterns = [
    # for /pages/
    path('', views.index, name='index'),    
    # for /pages/about/
    path('about/', views.about, name='about'),
    # for /polls/contact/
    path('contact/', views.contact, name='contact'),
]
